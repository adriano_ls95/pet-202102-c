package br.edu.cest.pet;

import java.util.ArrayList;
import java.util.List;

import br.edu.cest.pet.entity.staff.Funcionario;
import br.edu.cest.pet.entity.staff.Veterinario;
import br.edu.cest.pet.vo.EnTurno;

public class TesteLista {
	public static void main(String[] args) {
		List<Funcionario> lista = new ArrayList<Funcionario>();
		Veterinario v = new Veterinario("Josue", EnTurno.MAT, "123");
		v.setCpf("1111111");
		v.setSexo('M');

		Veterinario v2 = new Veterinario("Isabela", EnTurno.VESP, "456");
		v2.setCpf("2222222");
		v2.setSexo('M');

		lista.add(v2);
		lista.add(v);

		for (int i = 0; i < lista.size(); i++) {
			// ((Funcionario) objeto )
			Funcionario f = (Funcionario) lista.get(i);
			String nome = f.getNome();
			String cpf = f.getCpf();
			System.out.println("Nome:" + nome);
			System.out.println("CPF:" + nome);
		}
		System.out.println("-----------------");
		System.out.println("Usando generics");
		for (Funcionario f: lista) {
			//String nome = f.getNome();
			//String cpf = f.getCpf();
			System.out.println(f);
		}
	}
}
