package br.edu.cest.pet.divisao;

import java.util.ArrayList;

import br.edu.cest.pet.entity.staff.Funcionario;

public class Clinica extends Setor {
	ArrayList<Funcionario> staff;
	
	@Override
	public String getDivisao() {
		return "Clinica";
	}

	@Override
	public ArrayList<Funcionario> getStaff() {
		return staff;
	}


}
