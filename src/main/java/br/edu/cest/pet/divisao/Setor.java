package br.edu.cest.pet.divisao;

import java.util.ArrayList;

import br.edu.cest.pet.entity.staff.Atendente;
import br.edu.cest.pet.entity.staff.Funcionario;

public abstract class Setor {
	private Funcionario chefeDivisao;

	//getDivisao
	public abstract String getDivisao();
	
	//Lista Funcionario
	public abstract ArrayList<Funcionario> getStaff();
 	
	public Funcionario getChefeDivisao() {
		return chefeDivisao;
	}
	
	public void setChefeDivisao(Funcionario chefeDivisao) {
		this.chefeDivisao = chefeDivisao;
	}
	
	
	
}
