package br.edu.cest.pet.divisao;

import java.util.ArrayList;

import br.edu.cest.pet.entity.staff.Funcionario;

public class RH extends Setor {
	ArrayList <Funcionario> staff;
	
	@Override
	public String getDivisao() {
		return "RH";
	}

	@Override
	public ArrayList<Funcionario> getStaff() {
		return staff;
	}
	
}
