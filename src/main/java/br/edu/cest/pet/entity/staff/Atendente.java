package br.edu.cest.pet.entity.staff;

import br.edu.cest.pet.vo.EnTurno;

public class Atendente extends Funcionario {
	
	public Atendente(String nome, EnTurno turno, String registro) {
		super(nome, turno, registro);
	}
	
}
