package br.edu.cest.pet.entity;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

public class Colecao {
	public static void main(String[] args) {
		
		// [] = array
		String[] arrayStrings = new String[4];
		arrayStrings[0] = "Raimundo";
		arrayStrings[1] = "Romario";
		arrayStrings[2] = "Adriano";
		arrayStrings[3] = "Jose";
								//    0          1           2          3
		System.out.println("nome:" + arrayStrings[2]); // 3o elemento
		
		arrayStrings[2] = "William";
		
		System.out.println("nome:" + arrayStrings[2]); // 3o elemento
		
		System.out.println("nome:" + arrayStrings[3]); //4o elemento
		
		System.out.println("laco for");
		for (int j=0;j<arrayStrings.length;j++) { //j=4
			System.out.println("elemento:" + arrayStrings[j]);	// j=0
		}
		
		String[] arrayStr = new String[5];
		
		for (int j=0;j<arrayStrings.length;j++) { //j=4
			arrayStr[j]=arrayStrings[j];
		}
		
		arrayStr[4] = "Milla";
		arrayStrings = arrayStr;
		
		List listaStrings = new ArrayList();
		listaStrings.add("Joshua");
		listaStrings.add("Manoel");
		listaStrings.add("Francisco");
		
		System.out.println(listaStrings.size());
		System.out.println(listaStrings.get(0));
		for (int j=0;j<listaStrings.size();j++) {
			if (listaStrings.get(j).equals("Manoel" )) {
				
				System.out.println("Encontrei Manoel no indice:" + j);
				
			}
		}
	
		
	}
}
