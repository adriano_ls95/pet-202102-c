package br.edu.cest.pet.vo;

public class Locomocao {
	
	private String tipo;
	private String descricao;
		
	public Locomocao(String tipo, String descricao) {
		super();
		this.tipo = tipo;
		this.descricao = descricao;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
